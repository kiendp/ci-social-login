<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		require_once APPPATH.'third_party/google/Google_Client.php';
		require_once APPPATH.'third_party/google/contrib/Google_Oauth2Service.php';

		if (isset($_SESSION['userProfile'])) redirect('user/profile');
	}

	public function index()
	{
		$this->load->view('signin');
	}

	public function signinGoogle()
	{
		$clientId = '';
		$clientSecret = '';
		$redirectURL = base_url('signin/signinGoogle');
		//Call Google API
		$gClient = new Google_Client();
		$gClient->setApplicationName('signinGoogle');
		$gClient->setClientId($clientId);
		$gClient->setClientSecret($clientSecret);
		$gClient->setRedirectUri($redirectURL);
		$gOAuth2 = new Google_Oauth2Service($gClient);

		if(isset($_GET['code'])) {
			$gClient->authenticate($_GET['code']);
			$_SESSION['token'] = $gClient->getAccessToken();
			header('Location: ' . filter_var($redirectURL, FILTER_SANITIZE_URL));
		}

		if (isset($_SESSION['token'])) {
			$gClient->setAccessToken($_SESSION['token']);
		}
		
		if ($gClient->getAccessToken()) {
			$userProfile = $gOAuth2->userinfo->get();
			$_SESSION['userProfile'] = $userProfile;
			redirect('user/profile');
		} else {
			$url = $gClient->createAuthUrl();
			header("Location: $url");
			exit;
		}
	}

	public function signoutGoogle()
	{
		unset($_SESSION['userProfile']);
		unset($_SESSION['token']);
		$gClient = new Google_Client();
		$gClient->revokeToken();
		redirect('signin','refresh');
	}

	public function signinFb()
	{
		$userProfile = json_decode($this->input->post('userData'));
		return true;
	}
}