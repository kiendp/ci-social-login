<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		if (!isset($_SESSION['userProfile'])) redirect('signin');
	}

	public function profile()
	{
		$this->load->view('profile');
	}

	public function another()
	{
		$this->load->view('another');
	}

}