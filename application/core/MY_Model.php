<?php defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends CI_Model {
	protected $table;

	function __construct(){
		$this->load->database();
	}

	/**
	 * get data with custom SELECT and WHERE clause
	 * @method get_data
	 * @param  mixed   $conditions	[if $condition = 1 then get all data (default)]
	 * @param  mixed   $selections	[get all on default]
	 * @param  int     $limit
	 * @param  int     $offset
	 *
	 * e.g:
	 * $condition = ['key' => 'value', ...] then WHERE clase like "'key' = 'value' AND 'key' = 'value'"
	 * $condition = "'key1' = 'value1' AND/OR/... 'key2' like '%value2%'" then WHERE is same
	 * you can do this with $selections
	 * $selections = ['field1', 'field2', ...]
	 * $selections = 'field1, field2, ...'
	 */
	public function get_data($conditions = 1, $selections = '*', $limit = null, $offset = 0)
	{
		$this->db->select($selections);
		if($conditions != 1) {
			if(is_array($conditions)) {
				foreach ($conditions as $key => $value) {
					$this->db->where($key, $value);
				}
			} else $this->db->where($conditions);
		}
		$rs = $this->db->get($this->table, $limit, $offset);
		return $rs->result_array(); 
	}
	
	/**
	 * delete ONE data, using "=" or "LIKE" operator
	 * delete MULTIPLE data, using "IN" operator
	 * @method delete_data
	 * @param  array $conditions ['condition' => array(value1, value2, ...)]
	 * @param string $operator [= (default), LIKE, IN]
	 */
	public function delete_data($conditions, $operator = '')
	{
		switch ($operator) {
			case '':
			foreach ($conditions as $key=> $value) {
				$this->db->where($key, $value);
			}
			break;
			case 'like':
			foreach ($conditions as $key=> $value) {
				$this->db->like($key, $value);
			}
			break;
			case 'in':
			foreach ($conditions as $key=> $value) {
				$this->db->where_in($key, $value);
			}
			break;
		}

		return $this->db->delete($this->table);
	}

	/**
	 * insert when null and update if existed
	 * @method save_data
	 * @param  array    $data ['key' => 'value']
	 */
	public function save_data($data)
	{
		$query = "INSERT INTO $this->table ("; $tmp = "";
		$keys = array_keys($data);
		$values = array_values($data);

		for ($i = 0; $i < count($keys); $i++) { 
			$query .= "$keys[$i], ";
			$tmp .= "'$values[$i]', ";
		}
		$query = preg_replace('#(, )$#', ') VALUES (', $query);
		$query .= $tmp;
		$query = preg_replace('#(, )$#', ') ON DUPLICATE KEY UPDATE ', $query);
		foreach ($data as $key => $value) {
			$query .= "$key = '$value', ";
		}
		$query = preg_replace('#(, )$#', '', $query);

		$this->db->query($query);
		return $this->db->affected_rows();
	}
}
