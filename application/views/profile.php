<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sign in</title>
	<link rel="stylesheet" href="<?= base_url('vendor/css/bootstrap.min.css') ?>">
	<style>
	.container {
		height: 100px;
	}
	img {
		width: 150px !important;
	}
</style>
</head>
<body>
	<div class="container text-center">
		<div class="card text-left offset-md-4 col-md-4 mb-5 mt-5">
			<img class="card-img-top col-md-12 offset-md-3" src="<?= $_SESSION['userProfile']['picture'] ?>" alt="Picture profile">
			<div class="card-body">
				<p class="card-text">ID: <?= $_SESSION['userProfile']['id'] ?></p>
				<p class="card-text">Name: <?= $_SESSION['userProfile']['name'] ?></p>
				<p class="card-text">Email: <?= $_SESSION['userProfile']['email'] ?></p>
			</div>
		</div>
		<a href="<?= base_url('user/another') ?>" class="btn btn-default">go to another page</a>
		<a href="<?= base_url('signin/signoutGoogle') ?>" class="btn btn-primary">Sign out</a>
	</div>
</body>
</html>

