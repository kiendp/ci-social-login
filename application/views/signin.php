<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sign in</title>
	<link rel="stylesheet" href="<?= base_url('vendor/css/bootstrap.min.css') ?>">
	<style>
	.container {
		height: 100px;
		line-height: 100px;
	}
</style>
</head>
<body>
	<div class="container">
		<a href="<?= base_url('signin/signinGoogle') ?>" class="btn btn-danger">Sign in with Google+</a>
		<a href="javascript:void(0)" onclick="fbLogin();" class="btn btn-primary" id="fbLink">Sign in with Facebook</a>
		<div id="status"></div>
		<div id="userData"></div>
	</div>
	<script src="<?= base_url('vendor/js/jquery-3.2.1.min.js') ?>"></script>
	<script>
		window.fbAsyncInit = function() {
		    // FB JavaScript SDK configuration and setup
		    FB.init({
		    	appId      : '241683859808347',
		    	cookie     : true,
		    	xfbml      : true,
		    	version    : 'v2.10'
		    });
		    
		    // Check whether the user already logged in
		    FB.getLoginStatus(function(response) {
		    	if (response.status === 'connected') {
		    		getFbUserData();
		    	}
		    });
		 };

		// Load the JavaScript SDK asynchronously
		(function(d, s, id) {
			var js, fjs = d.getElementsByTagName(s)[0];
			if (d.getElementById(id)) return;
			js = d.createElement(s); js.id = id;
			js.src = "//connect.facebook.net/en_US/sdk.js";
			fjs.parentNode.insertBefore(js, fjs);
		}(document, 'script', 'facebook-jssdk'));

		// Facebook login with JavaScript SDK
		function fbLogin() {
			FB.login(function (response) {
				if (response.authResponse) {
					getFbUserData();
				} else {
					document.getElementById('status').innerHTML = 'User cancelled login or did not fully authorize.';
				}
			}, {scope: 'email'});
		}

		// Fetch the user profile data from facebook
		function getFbUserData(){
			FB.api('/me', {locale: 'en_US', fields: 'id,first_name,last_name,email,link,gender,locale,picture,cover'},
				function (response) {
					document.getElementById('fbLink').setAttribute("onclick","fbLogout()");
					document.getElementById('fbLink').innerHTML = 'Sign out from Facebook';
					document.getElementById('status').innerHTML = 'You are signing in, ' + response.first_name + '!';
					document.getElementById('userData').innerHTML = '<div style="position: relative;"><img src="'+response.cover.source+'" /><img style="position: absolute; top: 90%; left: 25%;" src="'+response.picture.data.url+'"/></div><p><b>FB ID:</b> '+response.id+'</p><p><b>Name:</b> '+response.first_name+' '+response.last_name+'</p><p><b>Email:</b> '+response.email+'</p><p><b>Gender:</b> '+response.gender+'</p><p><b>Locale:</b> '+response.locale+'</p><p><b>Profile Link:</b> <a target="_blank" href="'+response.link+'">click to view profile</a></p>';

		        // Save user data
		        saveUserData(response);
		     });
		}

		// Save user data to the database
		function saveUserData(userData){
			$.post("<?php echo base_url('signin/signinFb'); ?>", {oauth_provider:'facebook', userData: JSON.stringify(userData)}, function(data){ return true; });
		}

		// Logout from facebook
		function fbLogout() {
			FB.logout(function() {
				document.getElementById('fbLink').setAttribute("onclick","fbLogin()");
				document.getElementById('fbLink').innerHTML = 'Sign in with Facebook';
				document.getElementById('userData').innerHTML = '';
				document.getElementById('status').innerHTML = 'You have successfully signed out from Facebook.';
			});
		}
	</script>
</body>
</html>