<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
	<link rel="stylesheet" href="<?= base_url('vendor/css/bootstrap.min.css') ?>">
	<link rel="stylesheet" href="<?= base_url('vendor/css/font-awesome.min.css') ?>">
	<script src="<?= base_url('vendor/js/bootstrap.min.js') ?>"></script>
	<script src="<?= base_url('vendor/js/jquery-3.2.1.min.js') ?>"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<h1 class="display-1">Pham Trung Kien</h1>
		</div>
	</div>
	<div class="container">
		<div class="row">
			<div class="card col-sm-4" style="width: 18rem;">
				<div class="card-body">
					<h5 class="card-title">Card title</h5>
					<h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					<a href="#" class="card-link"><i class="fa fa-address-card"></i> Card link</a>
					<a href="#" class="card-link"><i class="fa fa-check"></i> Another link</a>
				</div>
			</div>
			<div class="card col-sm-4" style="width: 18rem;">
				<div class="card-body">
					<h5 class="card-title">Card title</h5>
					<h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					<a href="#" class="card-link"><i class="fa fa-address-card"></i> Card link</a>
					<a href="#" class="card-link"><i class="fa fa-check"></i> Another link</a>
				</div>
			</div>
			<div class="card col-sm-4" style="width: 18rem;">
				<div class="card-body">
					<h5 class="card-title">Card title</h5>
					<h6 class="card-subtitle mb-2 text-muted">Card subtitle</h6>
					<p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
					<a href="#" class="card-link"><i class="fa fa-address-card"></i> Card link</a>
					<a href="#" class="card-link"><i class="fa fa-check"></i> Another link</a>
				</div>
			</div>
		</div>
		<div class="row" style="margin-top: 50px;text-align: center;">
			<div class="col-md-12">
				<div class="alert alert-info" role="alert" id="alert"></div>
			</div>
		</div>
	</div>
	<script>
		$(function() {
			$('#alert').append('Test successful jquery!!!')
		});
	</script>
</body>
</html>